package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
	"github.com/xeipuuv/gojsonschema"
)

func wsEndpoint(w http.ResponseWriter, r *http.Request) {

	if r.Header.Get("Connection") == "Upgrade" {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }

		// upgrade this connection to a WebSocket
		// connection
		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
		}
		// err = ws.WriteMessage(1, []byte("Hi Client!"))
		// if err != nil {
		// 	log.Println(err)
		// }
		reader(ws)
	} else {
		fmt.Fprintf(w, "Home Page")
	}
}

// define a reader which will listen for
// new messages being sent to our WebSocket
// endpoint
func reader(conn *websocket.Conn) {
	//var player Player
	for {
		// read in a message
		_, p, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}

		err = isMessageValid(string(p))
		// Message we were given was invalid in some way, tell the client the issue
		if err != nil {
			sendBytes([]byte(err.Error()), conn)
		}

	}
}

// Send some bytes to the client
func sendBytes(bytes []byte, conn *websocket.Conn) (errr error) {
	if err := conn.WriteMessage(websocket.TextMessage, bytes); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func parseMessage(message string) {

}

func isMessageValid(message string) (reason error) {
	schemaLoader := gojsonschema.NewReferenceLoader("file:///home/kandern/projects/openlobby/schemas/entrypoint.json")
	jsonLoader := gojsonschema.NewStringLoader(message)
	result, err := gojsonschema.Validate(schemaLoader, jsonLoader)
	if err != nil {
		panic(err.Error())
	}
	if result.Valid() {
		return nil
	} else {
		var sb strings.Builder
		sb.WriteString("Message failed Schema validation:\n")
		for _, desc := range result.Errors() {
			sb.WriteString(fmt.Sprintf("- \n", desc))
		}
		return errors.New(sb.String())
	}
}

func setupRoutes() {
	http.HandleFunc("/", wsEndpoint)
}

// We'll need to define an Upgrader
// this will require a Read and Write buffer size
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func listen(port string) {
	setupRoutes()
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
