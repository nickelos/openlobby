module gitlab.com/nickelos/openlobby

go 1.15

require (
	github.com/gorilla/websocket v1.5.0
	github.com/xeipuuv/gojsonschema v1.2.0
)
